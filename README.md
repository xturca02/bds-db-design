# Databáza Knižnice
Projekt vrámci predmetu BPC-BDS, Fakulta elektrotechniky a komunikačných technológií, Vysoké učení technické, Brno.

## Cieľ projektu
Vytvorenie databázového systému s možným praktickým využitím v knižniciach. Vytvorenie ER Diagramu a scrípt pre PostgreSQL a MySQL.

## Autori návrhu databázy, vytvorenie PostreSQL a MySQL
Miková Timea (231256), Klára Turčanová (227248)

## SQL Queries
Názorná ukážka queries podľa zadania aj s pdf dokumentáciou.
Autor: Klára Turčanová (227248)

Zimný semester 2021/22
